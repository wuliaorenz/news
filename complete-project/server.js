const express = require('express')
const bodyparser = require('body-parser')
const userRouter = require('./router/userRouter')
const path = require('path')
const db = require('./db/connect')
const app = express()
app.use('*',function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*'); //这个表示任意域名都可以访问，这样写不能携带cookie了。
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');//设置方法
    if (req.method == 'OPTIONS') {
      res.send(200); //在正常的请求之前，会发送一个验证，是否可以请求。
    }
    else {
      next();
    }
})

app.use(express.static(path.join(__dirname,'./public')))
app.use(bodyparser.urlencoded({extends:false}))
app.use(bodyparser.json())

app.use('/user',userRouter)

app.get('*',(req,res) => {
  res.sendFile(path.resolve(__dirname,'./public/index.html'))
})

app.listen(3000,()=>{
    console.log("server start")
})