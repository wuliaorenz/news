const mongoose = new require('mongoose')

let userSchema = new mongoose.Schema({
    mail:{type:String,required:true},
    pwd:{type:String,required:true},
    nickname:{type:String,required:true}
})

let User = mongoose.model('user',userSchema)

module.exports = User