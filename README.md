# news
服务器到期了！！！
#### 介绍
    通过axios请求阿里云平台新闻接口，获取最新新闻数据，按数据格式渲染页面，用户可按新闻频道查询新闻。<br/>
    使用vue-router导航“页面”，使用vuex管理数据状态，在加载数据时，切换加载动画组件，提升用户体验。<br/>
    实现了用户注册登录功能，后端使用express框架构建服务器,使用mongoose模块构建数据模型。<br/>
    通过promise对象处理数据，实现了登录注册及发送邮箱验证码接口。<br/>


#### 安装教程

1.  vue-development为开发模式下的vue源码，complete-project为生产环境下的vue文件和nodeJS源码<br/>
2.  若只运行vue项目。<br/>
    只下载vue-development即可，npm i 安装依赖，npm run serve即可运行。（所请求的用户功能接口已在后端配置cors，无跨域问题）<br/>

3.  若运行vue+node+mongo完整项目。<br/>
    1）下载并打开vue-development> src> services> config.js，修改APPCODE（通过阿里云“全国热门带正文新闻查询_易源数据”获取），修改IP为自己服务器地址（本地则为localhost），npm i安装依赖，npm run build生成dist文件夹。<br/>
    2）下载complete-project，npm i安装依赖，将生成的dist文件夹里面的内容替换掉public文件夹里面的内容<br/>
    3）mongodb，建立一个“us”数据库，在server.js所在文件夹下执行node server.js即可开启服务器，打开localhost:3000即可访问。<br/>

#### 参考教程
vue:    渡一教育 https://www.bilibili.com/video/BV15A411q7Vv <br/>
node：  千锋教育 https://www.bilibili.com/video/BV1d4411W7Fp <br/>

