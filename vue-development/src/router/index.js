import Vue from "vue"
import VueRouter from "vue-router"
import config from "./config"
import store from "../store"

Vue.use(VueRouter) //1、安装
var router = new VueRouter(config)

router.beforeEach((to,from,next)=>{ //全局路由守卫
    if(to.meta.auth){
        if(store.state.loginUser.isLoading){
            next({name:"Auth",query:{returnurl:to.fullPath}})
        }else if(store.state.loginUser.data){
            next()
        }else{
            next({name:"Login"})
        }
    }else{
        next()
    }
})

export default router