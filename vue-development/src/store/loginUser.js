import {login,loginOut,whoAmI} from "../services/userService"
export default {
    namespaced:true,
    state:{
        data:null, //当前登录用户的信息对象
        isLoading:false //是否正在加载
    },
    mutations:{
        setIsLoading(state,payload){
            state.isLoading = payload
        },
        setData(state,payload){
            state.data = payload
        }
    },
    actions:{
        async login(context,payload){
            context.commit("setIsLoading",true)
            var resp = await login(payload)
            if(resp.err === 0){
                //设置token
                var token = resp.data._id
                localStorage.setItem("token",token)

                context.commit("setData",resp.data)
            }
            context.commit("setIsLoading",false)
            return resp
        },
        async whoAmI(context){
            context.commit("setIsLoading",true)
            var resp = await whoAmI()
            var data = null
            if(resp && !resp.err){
                data = resp.data
            }
            context.commit("setData",data)
            context.commit("setIsLoading",false)
        },
        logOut(context){
            loginOut()
            context.commit("setData",null)
        }
    }
}