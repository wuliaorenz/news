import {getNewsChannels} from "../services/newsService"

export default {
    namespaced:true,
    state:{
        data:[],
        isLoading:false,
    },
    mutations:{
        //state:原来的状态
        //payload：载荷
        setIsLoading(state,payload){
            state.isLoading = payload
        },
        setData(state,payload){
            state.data = payload
        }
    },
    actions:{  //在此进行副作用操作，提交commit到mutation
        //副作用操作：外部数据，ajax，localstorage等
        async fetchDatas(context){
            context.commit("setIsLoading",true)
            var channels = await getNewsChannels()
            context.commit("setData",channels)
            context.commit("setIsLoading",false)
        }

    }
}