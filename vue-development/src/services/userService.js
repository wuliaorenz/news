import axios from "axios"
import {IP} from "./config"
//登录
export async function login(loginInfo){
    var resp = await axios.post(IP+"user/login",loginInfo)
    return resp.data
}

//使用保存的令牌从服务器换取登录信息
export async function whoAmI(){
    var token = localStorage.getItem("token")
    console.log(token);
    if(!token){return null}
    var resp = await axios.post(IP+"user/whoami",{_id:token})
    return resp.data
}

//登出
export async function loginOut(){
    localStorage.removeItem("token")
}

//注册
export async function reg(userInfo){
    var resp = await axios.post(IP+"user/reg",userInfo)
    return resp.data
}

//发送验证码
export async function sendcode(mail){
    var resp = await axios.post(IP+"user/sendcode",{mail})
    return resp.data
}
