import Vue from 'vue'
import App from './App.vue'
import router from "./router"
import store from "./store"

//一开始就获取频道数据
store.dispatch("channels/fetchDatas")
store.dispatch("loginUser/whoAmI")

new Vue({
  render: h => h(App),
  router,    //配置路由到vue实例中
  store
}).$mount('#app')
